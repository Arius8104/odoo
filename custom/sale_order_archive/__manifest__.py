# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Sales (Sales/Orders/Archvied Orders)',
    'version': '1.0',
    'category': 'Sales',
    'summary': 'Sales Orders Archive',
    'description': """Sales Orders Archive""",
    'depends': [],
    'data': ['views/sale_order_archive.xml'],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'LGPL-3',
}
