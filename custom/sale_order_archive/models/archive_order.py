from odoo import models, fields


class SaleOrderArchive(models.Model):
    _name = 'sale.order.archive'
    _description = "Sale Order Archive"
    _order = 'date_order desc, id desc'
    _check_company_auto = True

    name = fields.Char(string='Order Reference', required=True, copy=False, readonly=True, states={'draft': [('readonly', False)]}, index=True, default=lambda self: _('New'))
    create_date = fields.Datetime(string='Creation Date', readonly=True, index=True, help="Date on which sales order is created.")
    partner_id = fields.Many2one(
        'res.partner', string='Customer', readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
        required=True, change_default=True, index=True, tracking=1,
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]",)
    user_id = fields.Many2one('res.users', string='SalePerson', index=True, tracking=2, default=lambda self: self.env.user,
        domain=lambda self: [('groups_id', 'in', self.env.ref('sales_team.group_sale_salesman').id)])
    amount_total = fields.Monetary(string='Total', store=True, readonly=True, compute='_amount_all', tracking=4)
    currency_id = fields.Many2one("res.currency", string="Order Currency")
    order_lines = fields.Integer(string='Count of order lines', readonly=True, required=False)
